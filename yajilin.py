#!/usr/bin/env python3

import argparse

from pysat.solvers import Glucose3
from pysat.card import CardEnc


class Puzzle:
    """A Yajilin puzzle object."""
    def __init__(self, text):
        # parse row and column counts
        lines = text.splitlines()
        try:
            self.rows = int(lines[0].strip())
            self.cols = int(lines[1].strip())
        except ValueError:
            raise ValueError('size could not be parsed')

        # ensure right number of rows
        if len(lines) - 2 != self.rows:
            raise ValueError('too many rows')

        # read off rows
        self.cells = []
        for index, row in enumerate(lines[2:]):
            row_cells = row.split()

            # ensure right number of entries in row
            if len(row_cells) != self.cols:
                raise ValueError(f'row {index} has too many entries')
            self.cells.append(row_cells)

    @staticmethod
    def _make_var_grid(rows, cols, start_value=1):
        grid = []
        for row_index in range(rows):
            row = [start_value + cols * row_index + col_index
                   for col_index in range(cols)]
            grid.append(row)
        return grid, start_value + rows * cols

    @staticmethod
    def _read_soln_grid(model, rows, cols, start_index=0):
        index = start_index
        soln_grid = []
        for row in range(rows):
            soln_row = []
            for col in range(cols):
                try:
                    soln_row.append(model[index] > 0)
                except IndexError:
                    soln_row.append(False)
                index += 1
            soln_grid.append(soln_row)
        return soln_grid, start_index + rows * cols


    @staticmethod
    def _parse_indicator(indicator):
        try:
            count = int(indicator[:-1])
            direction = indicator[-1]
        except (ValueError, KeyError):
            raise ValueError(f'indicator "{indicator}" '
                             'could not be parsed')
        return direction, count

    def _get_indicator_vars(self, row, col, direction):
        dirs = {'n': (-1, 0),
                'e': (0, 1),
                's': (1, 0),
                'w': (0, -1)}

        drow, dcol = dirs[direction]
        ref_vars = []
        cur_row, cur_col = row + drow, col + dcol
        while (0 <= cur_row < self.rows and
               0 <= cur_col < self.cols):

            # Note that we negate, as the count counts blocks
            ref_vars.append(-self.cell_vars[cur_row][cur_col])
            cur_row += drow
            cur_col += dcol

        return ref_vars

    def _add_indicative_count_cons(self):
        for row in range(self.rows):
            for col in range(self.cols):
                indicator = self.cells[row][col]
                if indicator != '-':
                    direction, count = self._parse_indicator(indicator)
                    indicator_vars = self._get_indicator_vars(
                        row, col, direction)
                    cons = CardEnc.equals(lits=indicator_vars, bound=count)
                    self.solver.append_formula(cons.clauses)

    def _add_consecutive_block_cons(self):
        for row in range(self.rows):
            for col in range(self.cols):
                if self.cells[row][col] != '-':
                    continue
                cur_var = self.cell_vars[row][col]
                if cur_var > 11:
                    break
                if row < self.rows - 1 and self.cells[row + 1][col] == '-':
                    down_var = self.cell_vars[row + 1][col]
                    cons = CardEnc.atleast(lits=[cur_var, down_var], bound=1)
                    self.solver.append_formula(cons.clauses)
                if col < self.cols - 1 and self.cells[row][col + 1] == '-':
                    right_var = self.cell_vars[row][col + 1]
                    cons = CardEnc.atleast(lits=[cur_var, right_var], bound=1)
                    self.solver.append_formula(cons.clauses)

    def _add_wall_pipe_cons(self):
        for row in range(self.rows):
            for col in range(self.cols - 1):
                left_var = self.cell_vars[row][col]
                right_var = self.cell_vars[row][col + 1]
                wall_var = self.horiz_vars[row][col]
                self.solver.add_clause([left_var, -wall_var])
                self.solver.add_clause([right_var, -wall_var])

        for row in range(self.rows - 1):
            for col in range(self.cols):
                top_var = self.cell_vars[row][col]
                bottom_var = self.cell_vars[row + 1][col]
                wall_var = self.vert_vars[row][col]
                self.solver.add_clause([top_var, -wall_var])
                self.solver.add_clause([bottom_var, -wall_var])

    def _add_in_out_wall_cons(self):
        for row in range(self.rows):
            for col in range(self.cols):
                wall_vars = []
                if row > 0:
                    wall_vars.append(self.vert_vars[row - 1][col])
                if row < self.rows - 1:
                    wall_vars.append(self.vert_vars[row][col])
                if col > 0:
                    wall_vars.append(self.horiz_vars[row][col - 1])
                if col < self.cols - 1:
                    wall_vars.append(self.horiz_vars[row][col])

                # TODO: figure out how to OR these!
                wall_cons = CardEnc.equals(lits=wall_vars, bound=0)
                pipe_cons = CardEnc.equals(lits=wall_vars, bound=2)
                # self.solver.append_formula(cons)

    def _parse_solution(self):
        model = self.solver.get_model()
        start_index = 0

        self.cell_soln, start_index = self._read_soln_grid(
            model, self.rows, self.cols)
        self.horiz_soln, start_index = self._read_soln_grid(
            model, self.rows, self.cols - 1, start_index=start_index)
        self.vert_soln, start_index = self._read_soln_grid(
            model, self.rows - 1, self.cols, start_index=start_index)

    def solve(self):
        # set solver
        self.solver = Glucose3()

        # create variables for cells
        self.cell_vars, start_value = self._make_var_grid(self.rows, self.cols)

        # create variables for horizontal walls
        self.horiz_vars, start_value = self._make_var_grid(
            self.rows, self.cols - 1, start_value=start_value)

        # create variables for vertical walls
        self.vert_vars, start_value = self._make_var_grid(
            self.rows - 1, self.cols, start_value=start_value)

        # ensure indicative cells are accurate
        self._add_indicative_count_cons()

        # ensure no two blocks touch
        self._add_consecutive_block_cons()

        # ensure pipes through walls enter cells
        self._add_wall_pipe_cons()

        # ensure no pipes on indicative cells
        pass

        # ensure each pipe block has exactly two wall connections
        self._add_in_out_wall_cons()

        # solve
        self.solver.solve()
        self._parse_solution()

        # ensure there's a single loop
        pass

    def pretty_print_soln(self):
        lines = []
        for row in range(self.rows):
            # print above the row
            line = ''
            for col in range(self.cols):
                if row > 0 and self.vert_soln[row - 1][col]:
                    line += '. | '
                else:
                    line += '.   '
            line += '.'
            lines.append(line)

            # print the row itself
            line = ' '
            for col in range(self.cols):
                if self.cell_soln[row][col]:
                    if col > 0 and self.horiz_soln[row][col - 1]:
                        line += '-+'
                    else:
                        line += ' +'
                else:
                    cell = self.cells[row][col]
                    if cell == '-':
                        line += ' #'
                    else:
                        if len(cell) > 2:
                            cell = cell[:2]
                        cell += (2 - len(cell)) * ' '
                        line += cell
                if col < self.cols - 1 and self.horiz_soln[row][col]:
                    line += '--'
                else:
                    line += '  '
            line += '  '
            lines.append(line)

        # print last line
        last_line = '.   ' * self.cols + '.'
        lines.append(last_line)
        return '\n'.join(lines)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Solve a Yajilin puzzle.')
    parser.add_argument('puzzle', help='puzzle to solve, in Copris format')
    args = parser.parse_args()

    with open(args.puzzle, 'r') as f:
        text = f.read()

    puzzle = Puzzle(text)
    puzzle.solve()
    print(puzzle.pretty_print_soln())

